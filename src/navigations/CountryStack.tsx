import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import CountryScreen from '../screens/CountryScreen'
import { SCREEN } from '../utils/constant'

const Stack = createNativeStackNavigator()

const CountryStack = () => {
  return (
    <Stack.Navigator
      initialRouteName={SCREEN.CountryScreen}
      screenOptions={{ headerShown: false }}>
      <Stack.Screen name={SCREEN.CountryScreen} component={CountryScreen} />
      {/* <Stack.Screen
        name={SCREEN.CountryDetailScreen}
        component={CountryDetailScreen}
        options={() => ({
          headerShown: true,
          headerTitle: 'Country',
        })}
      /> */}
    </Stack.Navigator>
  )
}

export default CountryStack
