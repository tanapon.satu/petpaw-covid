import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import React from 'react'
import Icon from 'react-native-vector-icons/FontAwesome'
import AboutAppScreen from '../screens/AboutAppScreen'
import SummaryScreen from '../screens/SummaryScreen'
import { SCREEN, STACK } from '../utils/constant'
import CountryStack from './CountryStack'
// import { Icon } from 'react-native-elements'

const Tab = createBottomTabNavigator()

const HomeStack = () => {
  return (
    <Tab.Navigator screenOptions={{ headerShown: false }}>
      <Tab.Screen
        name="Summary"
        component={SummaryScreen}
        options={{
          tabBarIcon: () => <Icon name="globe" size={25} />,
        }}
      />
      <Tab.Screen
        name="Country"
        component={CountryStack}
        options={{
          tabBarIcon: () => <Icon name="flag" size={25} />,
        }}
      />
      <Tab.Screen
        name="About"
        component={AboutAppScreen}
        options={{
          tabBarIcon: () => <Icon name="bars" size={25} />,
        }}
      />
    </Tab.Navigator>
  )
}

export default HomeStack
