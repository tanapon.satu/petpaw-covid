import React from 'react'
import {
  getFocusedRouteNameFromRoute,
  NavigationContainer,
} from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import HomeStack from './HomeStack'
import FeedBackScreen from '../screens/FeedBackScreen'
import { Button, Pressable, Text } from 'react-native'
import { useRecoilValue } from 'recoil'
import { SCREEN, STACK } from '../utils/constant'
import CountryDetailScreen from '../screens/CountryDetailScreen'

const Stack = createNativeStackNavigator()

const AppNavigator = () => {

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Group screenOptions={{ headerShown: true }}>
          <Stack.Screen
            name={STACK.HomeStack}
            component={HomeStack}
            options={({ route, navigation }) => {
              const routeName =
                getFocusedRouteNameFromRoute(route) ?? SCREEN.SummaryScreen

              const headerTitle = {}
              switch (routeName) {
                case STACK.CountryStack: {
                  Object.assign(headerTitle, {
                    headerTitle: 'Country',
                  })
                  break
                }
                case SCREEN.AbountAppScreen: {
                  Object.assign(headerTitle, {
                    headerTitle: 'About',
                  })
                  break
                }
                case SCREEN.SummaryScreen:
                default: {
                  Object.assign(headerTitle, {
                    headerTitle: 'Summary',
                  })
                }
              }

              return {
                ...headerTitle,
                headerRight: () => (
                  <Pressable
                    onPress={() => navigation.navigate(SCREEN.FeedBackScreen)}>
                    <Text>FeedBack</Text>
                  </Pressable>
                ),
              }
            }}
          />
          <Stack.Screen
            name={SCREEN.CountryDetailScreen}
            component={CountryDetailScreen}
            options={() => ({
              headerShown: true,
              headerTitle: 'CountryDetail',
            })}
          />
        </Stack.Group>
        <Stack.Group screenOptions={{ presentation: 'modal' }}>
          <Stack.Screen
            name={SCREEN.FeedBackScreen}
            component={FeedBackScreen}
          />
        </Stack.Group>
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default AppNavigator
