import axios, { AxiosResponse } from 'axios'
import { ICaseResponse, ICountry } from './interface'

const covidApi = axios.create({
  baseURL: 'https://covid-api.mmediagroup.fr/v1/cases',
})

const countryApi = axios.create({
  baseURL: 'https://restcountries.eu/rest/v2/all',
})

export const getAllCasesService = (): Promise<AxiosResponse<ICaseResponse>> => {
  return covidApi.get('')
}

export const getCaseByCountryService = (
  country: string = 'Thailand',
): Promise<AxiosResponse<ICaseResponse>> => {
  return covidApi.get(`?country=${country}`)
}

export const getAllCountriesService = (): Promise<
  AxiosResponse<ICountry[]>
> => {
  return countryApi.get('')
}
