export enum SCREEN {
  SummaryScreen = 'SummaryScreen',
  CountryScreen = 'CountryScreen',
  CountryDetailScreen = 'CountryDetailScreen',
  AbountAppScreen = 'AbountAppScreen',
  FeedBackScreen = 'FeedBackScreen',
}

export enum STACK {
  HomeStack = 'HomeStack',
  CountryStack = 'CountryStack',
}

// https://dev.to/younginnovations/get-instant-country-flags-22pe
export const baseCountryFlagApi = (code: string) =>
  `https://www.countryflags.io/${code}/shiny/64.png`
