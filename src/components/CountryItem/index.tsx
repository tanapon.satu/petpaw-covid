import { useNavigation } from '@react-navigation/native'
import React from 'react'
import {
  View,
  Text,
  ImageBase,
  Image,
  StyleSheet,
  Pressable,
} from 'react-native'
import { Circle, SvgUri } from 'react-native-svg'
import { baseCountryFlagApi, SCREEN } from '../../utils/constant'
import { ICountry } from '../../utils/interface'

interface CountryItemProps {
  country: ICountry
  onPress: () => {}
}

const CountryItem: React.FC<CountryItemProps> = ({ country, onPress }) => {
  const navigation = useNavigation()

  return (
    <Pressable style={styles.container} onPress={onPress}>
      <View style={styles.logoContainer}>
        <Image
          style={styles.logo}
          source={{ uri: baseCountryFlagApi(country.alpha2Code) }}
        />
        {/* <SvgUri
          width="100%"
          height="100%"
          style={styles.logo}
          uri={country.flag}
        /> */}
      </View>
      {/* <Text style={styles.name}>{country.flag}</Text> */}
      <Text style={styles.name}>{country.name}</Text>
    </Pressable>
  )
}

const styles = StyleSheet.create({
  container: {
    // backgroundColor: 'blue',
    flexDirection: 'row',
    margin: 10,
    // flex: 1,
    alignItems: 'center',
  },
  logoContainer: {
    // backgroundColor: 'red',
    margin: 2,
    width: 60,
    height: 50,
    maxHeight: 120,
    maxWidth: 120,
    // borderRadius: 50,
    overflow: 'hidden',
    // alignItems: 'center',
  },
  logo: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },
  name: {
    fontSize: 20,
  },
})

export default CountryItem
