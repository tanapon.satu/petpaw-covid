import * as React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { useRecoilState, useRecoilValue } from 'recoil'
import { fontState } from '../../recoils'
import { numberWithCommas } from '../../utils/common'

interface StatItemProps {
  label: string
  data: number
  bgColor?: string
}

const StatItem = ({ label, data = 0, bgColor = '#eea5a5' }: StatItemProps) => {
  const [font] = useRecoilState(fontState)
  return (
    <View
      style={[
        styles.container,
        {
          backgroundColor: bgColor,
        },
      ]}>
      <View style={styles.labelContainer}>
        <Text style={[styles.label, { fontSize: font.normal }]}>{label}</Text>
      </View>
      <View style={styles.statContainer}>
        <Text style={[styles.stat, { fontSize: font.large }]}>
          {numberWithCommas(data)}
        </Text>
      </View>
    </View>
  )
}

export default StatItem

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 8,
    margin: 10,
    minHeight: 200,
    justifyContent: 'space-evenly',
    // alignItems: 'center',
    borderRadius: 4,
    // height: 150,
    shadowColor: 'black',
    // width: 150,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  labelContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    // flex: 0.1,
    // height: 20,
  },
  label: {
    color: 'white',
    fontWeight: 'bold',
  },
  stat: {
    color: 'white',
    fontWeight: 'bold',
  },
  statContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    // flex: 0.9,
  },
})
