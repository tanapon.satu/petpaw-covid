import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const AboutAppScreen = () => {
  return (
    <View style={styles.container}>
      <Text style={{ fontSize: 20 }}>Covid19 Workshop</Text>
    </View>
  )
}

export default AboutAppScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
})
