import { useNavigation } from '@react-navigation/native'
import React, { useEffect, useMemo, useState } from 'react'
import {
  Button,
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  TextInputProps,
  View,
} from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { useSetRecoilState } from 'recoil'
import CountryItem from '../../components/CountryItem'
import { currentScreenState } from '../../recoils'
import { SCREEN } from '../../utils/constant'
import { ICountry } from '../../utils/interface'
import { getAllCountriesService } from '../../utils/services'

const CountryScreen = ({ navigation }) => {
  // const navigation = useNavigation()
  const [countries, setCountries] = useState<ICountry[]>([])
  const [filteredContries, setFilteredCountries] =
    useState<ICountry[]>(countries)

  useEffect(() => {
    const getAllCountries = async () => {
      const { data } = await getAllCountriesService()
      setCountries(data)
      setFilteredCountries(data)
    }

    getAllCountries()
  }, [])

  const onTextChanged = ({ nativeEvent: { text } }) => {
    if (text.length > 0) {
      const _filteredContries = countries.filter((country) =>
        country.name.includes(text),
      )
      setFilteredCountries(_filteredContries)
    } else {
      setFilteredCountries(countries)
    }
  }

  if (countries.length === 0) {
    return (
      <View style={styles.container}>
        <Text>Loading</Text>
      </View>
    )
  }

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        autoFocus={true}
        onChange={onTextChanged}
      />
      <FlatList
        data={filteredContries}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => {
          return (
            <CountryItem
              country={{
                name: item.name,
                flag: item.flag,
                alpha2Code: item.alpha2Code,
              }}
              onPress={() =>
                navigation.navigate(SCREEN.CountryDetailScreen, {
                  countryName: item.name,
                })
              }
            />
          )
        }}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#80858a',
  },
  input: {
    // height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    fontSize: 20,
  },
})

export default CountryScreen
