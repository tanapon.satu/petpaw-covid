import React from 'react'
import { Button, StyleSheet, Text, TextInput, View } from 'react-native'

interface FeedBackModal {
  navigation: any
}

const FeedBackScreen: React.FC<FeedBackModal> = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Text>FeedBack</Text>
      <TextInput style={styles.textInput} multiline={true} />
      <Button onPress={() => navigation.goBack()} title="Send" />
    </View>
  )
}

export default FeedBackScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
  },
  textInput: {
    margin: 10,
    height: 100,
    minWidth: '70%',
    maxWidth: '70%',
    borderWidth: 1,
  },
})
