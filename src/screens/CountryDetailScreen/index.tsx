import React, { useEffect, useState } from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'
import { useSetRecoilState } from 'recoil'
import StatItem from '../../components/StatItem'
import { currentScreenState } from '../../recoils'
import { SCREEN } from '../../utils/constant'
import { ISummary } from '../../utils/interface'
import { getCaseByCountryService } from '../../utils/services'

const CountryDetailScreen = ({ route }) => {
  const { countryName } = route.params
  const [cases, setCases] = useState<ISummary>()

  useEffect(() => {
    const getCaseByCountry = async () => {
      const { data } = await getCaseByCountryService(countryName)
      setCases(data.All)
    }

    getCaseByCountry()
  }, [])

  if (!cases) {
    return (
      <View style={styles.loading}>
        <Text style={{ fontSize: 30 }}>Loading</Text>
      </View>
    )
  }

  return (
    <View style={styles.container}>
      <StatItem label="Confirmed" data={cases.confirmed} bgColor="#eea5a5" />
      <StatItem label="Deaths" data={cases.deaths} bgColor="#e41313" />
      <StatItem label="Recovered" data={cases.recovered} bgColor="#3cb616" />
    </View>
  )
}

export default CountryDetailScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  loading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
