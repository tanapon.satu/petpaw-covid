import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import StatItem from '../../components/StatItem'
import { ISummary } from '../../utils/interface'
import { getCaseByCountryService } from '../../utils/services'

const SummaryScreen = () => {
  const [cases, setCases] = useState<ISummary>()

  useEffect(() => {
    const getAllCases = async () => {
      const { data } = await getCaseByCountryService()
      setCases(data.All)
    }

    getAllCases()
  }, [])

  if (!cases) {
    return (
      <View style={styles.loading}>
        <Text style={{ fontSize: 30 }}>Loading</Text>
      </View>
    )
  }

  return (
    <View style={styles.container}>
      <StatItem label="Confirmed" data={cases.confirmed} bgColor="#eea5a5" />
      <StatItem label="Deaths" data={cases.deaths} bgColor="#e41313" />
      <StatItem label="Recovered" data={cases.recovered} bgColor="#3cb616" />
    </View>
  )
}

export default SummaryScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  loading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
