import { atom } from 'recoil'

export const fontState = atom({
  key: 'fontState',
  default: {
    normal: 24,
    large: 30,
    weight: 'bold',
  },
})
